+++
title = "What the (c)heck"
description = "Checklist for code or peer reviews"
tags = [
    "Peer review",
    "Checklist",
]
date = "2017-10-07"
categories = [
    "Mendix",
]
menu = "main"
author = "Stephan Bruijnis"
+++

Code review or peer review is the process of systematically reviewing each others code/work for mistakes, errors and quality. It is a fundamental and highly effective process to increase quality in software development.

I use the following checklist and questions when reviewing the work of others and it has proven to be very useful:

▢ Objective based  
▢ Unbreakable  
▢ Security  
▢ Performance  
▢ Maintainability  
▢ Testable  
▢ Reusability

## Objective based
The code achieves its purpose. Does it complete the task? Does it work in the happy flow? Does it work with edge cases? Does it cause undesirable side-effects? Is the intent clear? If not, is it documented? Is there any incomplete code? Are there any TODO markers left?

## Unbreakable
Does it handle input well? Is input sanitized? Can it deal with invalid input? Can it deal with missing data and objects? Are responses handled correctly? Are error and response messages set with necessary details?

## Security
Is it secure in terms of authentication, roles, unauthorized access, SQL injections, cross-side scripting? OWASP Top 10?

## Performance
Does it still work with high volumes? Are asynchronous calls used? Are batches used? Are transactions used efficiently? Do loops have a set length and correct termination conditions?

## Maintainability
Is architecture followed throughout the application? Is the model readable? Does it follow conventions, standards, best practices and is consistent with the existing application? No design deviation? Is it a concise and consistent expression of ideas? Is it easy to extend or modify? No warnings or deprecations introduced?

## Testable
Is it covered by tests (unit, integration etc.)? Is logging available and configurable? Is the data traceable?

## Reusability
Is available code used effectively? Is the code as generalized/abstracted as it could be? Is the code a candidate for reusability? Are external libraries used? If so are these necessarry, safe, legally usable and compliant?
