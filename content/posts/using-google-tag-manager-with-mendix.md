+++
title = "Using Google Tag Manager with Mendix"
description = "Push events to the GTM datalayer to make analytics easy"
tags = [
    "Google Tag Manager",
    "GTM",
    "Analytics",
]
date = "2016-02-19"
categories = [
    "Mendix",
]
menu = "main"
author = "Stephan Bruijnis" 
+++

Analytics are essential in monitoring and reacting to traffic and usage of your application. The widget for Google Analytics in the Mendix Appstore lets you track events, pages and transactions, but can be time-consuming to configure. With Google Tag Manager you can launch new tags with a few clicks. Instead of adding new events in Mendix, the Google Tag Manager allows marketers to add new tags without the need for changes to the model and deployments of Mendix. This post is a description of my first-time setup of Google Tag Manager and Mendix, and it worked!

## Introduction
Google Tag Manager does not replace Google Analytics, it just makes it a lot easier to manage all the sites, tags, scripts etc. Especially when you have GTM set up for several other web sites and apps, adding your Mendix app to GTM can save you a lot of time managing analytics. So, when I was asked to connect Google Tag Manager to a pilot app, I was interested, because as far as I was concerned it wasn't done before. Thus, challenge accepted!

## Getting started
First you need to add the Google Tag Manager code to the index.html of your Mendix project. You will find this in you theme folder of your Mendix project. Add the code directly after the tag. Your code should look something like this:

```
<body dir="ltr">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-W2F3NB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM- XXXXXX');</script>
<!-- End Google Tag Manager -->
    <div id="content"></div>
    <script>
        dojoConfig = {
            baseUrl: "mxclientsystem/dojo/",
            cacheBust: "{{cachebust}}",
            rtlRedirect: "index-rtl.html"
        };
    </script>
    <script src="mxclientsystem/mxui/mxui.js?{{cachebust}}"></script>
</body>
```

Note the portion of the code GTM-XXXXXX: This is the unique identifier for each of your containers. Be sure to remove any existing Google Analytics tags and elements from your project and files.

You’ve now added GTM to your Mendix application. You can start managing tags in GTM. But wait there is more!

## Pushing data
Google Tag Manager has a little difficulty recognizing pages and navigation between these page, because Mendix is a one page application. So you need to give GTM some hints about the page you’re on. You might be thinking, I knew there is a catch, there is always a catch. But it also allows us to add some more value to the data gathered by GTM.

The API of GTM offers the dataLayer.push method to capture information such as values entered or selected in a form. Or to push the data of the Mendix page to GTM. Consider an user profile application with 3 pages: profile, calendar and user documents. To capture page visits and page specific data we need to add a HTMLSnippet to the Mendix pages we wish to track. The snippet contains the following HTML code: `<script type=""text/javascript"">dataLayer.push({'event' : 'stateChange', 'state' : 'profile'});</script>`

Replace the 'profile' with 'calendar' and 'documents' for the specific pages. In GTM you can add scripts and tags which will respond to this stateChange. And here comes the beauty since a dataLayer.push is added anyway, we might as well enrich it with some useful data. Consider the earlier example of an user profile application. The information is aggregated, all visits to the calendar is a sum of all visits, regardless of who’s calendar it is. The information would be more valuable if we would know which user’s calendar is visited how often.

## Hitting a bump
The HTMLsnippet actually needs to send more data then the static content. The newer versions of the HTMLsnippet widget also have a _with context_ version. During our project we (my colleague Chris) fixed it by making a specific widget combining the functionality of the HTMLsnippet widget and the FormatString widget* (because it has a context object, but unfortunatly or luckily - cuz security - doesn't execute Javascript code). With this custom widget attributes from the context object (e.g. containing the username) could be used, allowing us to push context data to the datalayer. The new code: `<script type="" text="" javascript""="">dataLayer.push({'event' : 'stateChange', 'state' : '${1}.calendar'});</script>`. The `${1}` contains a username without spaces, and in GTM a split is defined based on the dot between `${1}` and `calendar`.

## The results
The past month (January 2016) the GTM implementation has been providing the customer with very useful data and analytics on the usage of the Mendix app. It showed all the clicks and events they needed to determine behavior, gather user data, and compare results to the other sites in their analytics tool. The integration of Google Tag Manager and Mendix is a success!

## Troubleshooting
For a detailed description of how to implement Google tag Manager use the [GTM Developer Guide](https://developers.google.com/tag-manager/devguide) Furthermore troubleshooting can be done with the [Preview Mode](https://support.google.com/tagmanager/answer/6107056) and [Tag Assistant](https://support.google.com/tagassistant#topic=6000196) a Chrome Extension to help verify and troubleshoot your installation. It allows you to inspect the tags fired and defined.
