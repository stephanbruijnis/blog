+++
title = "Scheduled events might not work the way you would expect"
description = "Scheduled events are interval based."
tags = [
    "Scheduled Events",
    "Intervals",
]
date = "2016-08-12"
categories = [
    "Mendix",
]
menu = "main"
author = "Stephan Bruijnis" 
+++

Scheduled Events (SE) allow you to execute microflows at a specific time. These events can be repeated based on an interval (e.g. every day). The Mendix platform calculates when the next iteration of an event should occur based on fixed intervals. These intervals are configured by the developer but the execution of these intervals might not meet expectations. For example, an event scheduled at monthly interval will run every 31 days instead of every month.

## Fixed intervals
The Interval type (a repeat-property on a scheduled event) suggests that a scheduled event is repeated on the indicated interval, for example every # minutes, # days, #months, #years etc. All these interval types are fixed. A minutes equals 60 seconds, and an hour equals 60 minutes, a day is always 24 hours, a month 31 days and a year 365 days. This may not be as obvious as it seems... Because if you schedule an event to start _at March 1, it will run on April 1, May 2, Jun 2, Jul 3, Aug 3, Sep 3, etc._ [(mx)](https://docs.mendix.com/refguide6/Scheduled+Events). Furthermore; after setting our clocks on hour forward (in the spring) our day is actually 1 hour shorter, which causes daily scheduled events to run an hour later.

## Calculated
The platform schedules events with fixed intervals and performs calculations to determine when the next iterations/intervals of the event should run. The configured start of a SE is always used as base for the interval calculations made by the platform. Which means if you schedule an event to start at 15 January 12:00 UTC and repeat it on a daily interval, it will run at a 24 hours interval. Which means in summer time it will run at 14:00 CET (UTC +2) while in winter time it runs at 13:00 CET (UTC +1), regardless of server restarts. However, if you schedule an event based on server time (and that server uses a timezone such as CET) it depends on the start settings: does the start date occur in Daylight Saving Time or not? For example, an event scheduled to start at 15 January 12:00 Server Time (CET) would run today (August, 12) at 13:00 CET (since Daylight Time is observed). Set it to 15 May 12:00 Server Time (CET) and today it would run at 12:00 CET.

## Running at specific moments in time
These intervals make little sense to business users, they just want their e-mails to be sent at 16.00 (regardless of DST) or their monthly reports generated at the 1st day of the month. This is not hard to achieve, but deserves a little attention. Take a look at the expressions provided in the [Mendix documentation](https://world.mendix.com/display/refguide6/Scheduled+Events). In summary: it can be fixed by scheduling an event more often, and use an expression to determine whether the current day or time meets the requirements to run the event. To correct for DST one could use the same type of expressions provided in the documentation (use 'HH' or 'kk' in an exclusive split to determine the right hour).

Consider an event that must run every day at 12:05 (CET). _When scheduling an event to start at a certain UTC time, the platform technically won't have a problem, because UTC doesn't use daylight saving. However your users will still experience the event to run at a different hour._ The first option would be: scheduled the event every hour and check whether the CurrentDateTime it is the correct hour _(HH = 12)_. A second option would be to create two daily events one set at UTC 10:00 and one set at UTC 11:00, they both call a different microflow checking whether daylight time is active. Use the following Java code: `return TimeZone.getDefault().inDaylightTime(CurrentDateTime);`

The event scheduled at UTC 10:00 should call the main microflow when _inDaylightTime = true_ while the event scheduled at 11:00 should call the main microflow when _inDaylightTime = false_.

## Considerations
* A new scheduled event iteration will only start if the previous iteration of that specific SE is completed;
* A scheduled event interval which is shorter than the processing time of that event will trigger a new iteration immediately after its completion;
  * For example: The scheduled event run's every minute. The processing takes 1,5 minute. The first iteration will run at 14:15:00, a new iteration was scheduled during the processing at 14:16:00, but won't start because an instance of that scheduled event is already running. Instead it will run immediately after completion, thus 14:16:30, and the next one will run at 14:18:00, ...14:19:30 etc.
* Server restarts won't influence the scheduled event time - it uses the defined start in the model and calculates next iterations.
  * Update the start datetime of your SE (in the modeler) once in a while to speed up the iteration-calculation process 
  
## Alternatives
Events scheduled from the modeller can be replaced by run time configured microflows, each method has pro's and con's in terms of maintainability, logging, performance and features. Take a look at the Microflow Scheduler and the Scheduled Event Manager in the Mendix app store to see what else is out there.
