+++
title = "Writing better commit messages"
description = "Commit messages serves many purposes and can save everyone a great amount of time"
tags = [
    "Commmit message",
    "Why",
]
date = "2017-07-05"
categories = [
    "Mendix",
]
menu = "main"
author = "Stephan Bruijnis"
+++

A commit message is a communication between you and your co-developers. Creating a habit of writing good commit messages serves many purposes and can save everyone a great amount of time and frustration.

## The good, the bad and the ugly
A good commit message serves several purposes:
* Explanation of the commit (the what and why)
* Speed up the reviewing process
* Writing release notes
* Future maintenance and reference
* Information for merging

![xkcd git comments](https://imgs.xkcd.com/comics/git_commit.png)

Writing a good commit message does not happen by accident, however a bad and ugly one happens by default. I must admit if you look at commits I made, you will probably find messages ranging from "pewpew", "things" and "moar java" to beautiful ascii-art. But none of these messages are particularly helpful. Thus I decided to research how commit messages are handled by the git community and used it to improve my own commit messages.

Every software project is a collaboration project, even if you are working on your own. The 3 months from now you or 5 years from now you will need all the info on what was changed, why it was necessary and how it was approached. Because the train of thought has left the station.

## Best practices
Across the git community a consistent set of [best practices](https://chris.beams.io/posts/git-commit) can be found. Since these are proven best practices, I tried to adjust them towards the Mendix commit messages (SVN versioning tool). These are the main requirements of any git commit message:
* _Summary_ of what is changed no more than 50 characters
* Followed by a _description_ with a more detailed why (intention and implementation)

### Summary
The summary should be on the first line and less than 50 characters. This should explain what was changed in an _imperative present tense_ (e.g. fix, add, change). This will communicate context in a concise and consistent manner. If you have a hard time writing a summary of 50 characters or less you might be committing too many changes in one go. Consider committing more often.

Some examples:
<pre>Fix calendar timezone bug
Add tests for Account#create
Change file upload limit</pre>

Note: The Mendix modeler shows on most resolutions the first 64 characters of a commit message. The Sprintr - Team server shows up to 100 characters. Yet I would like to stick with 50 characters as a target, and 64 as a hard limit. This to create a habit that is more universal.

### Description
Use the following lines to explain the what, why and how of the change. In most cases, you can leave out details about how a change has been made. The Mendix models are most often self-explanatory in this regard, should they be too complex: that's what annotations are for. Focus on the why and how. _How did it work before the change? What was wrong with that? How does it work now? And why did you solve it like that?_ Bullet points can be helpful in summing up consequences or side effects of the change. However, the "changes in model" and "changes on disk" already show what was changed, so refrain from listing all the changed files.

## Further considerations
* Approach commit messages as naming conventions and development best practices: discuss them with your team, make them client specific if necessary
* Agree on how to reference data, e.g. Sprintr story ID's, support tickets and JIRA issues
* How do I know my commit message is good enough? Check if another developer could recreate your change given only your commit message

## Final thoughts
It may feel like bureaucracy and a waste of time at first, but once it is a habit you are building an _useful revision history_ which will speed up the reviewing process and writing of release notes, and offers context and explanations to future maintainers and developers.
