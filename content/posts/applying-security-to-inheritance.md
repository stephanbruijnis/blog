+++
title = "Applying security to inheritance"
description = "In case of inheritance the security of the subclass is applied"
tags = [
    "Security",
    "Inheritance",
]
date = "2017-06-01"
categories = [
    "Mendix",
]
menu = "main"
author = "Stephan Bruijnis"
+++

Configuring the security of a Mendix application is an important but sometimes tedious task. Misunderstanding of how security is applied by the Mendix platform can lead to failing security with great consequences. It is the developers task to constrain each entity with access rules. This post is a quick guide to setting up security in the context of inheritance.

Knowledge of [How to: Create a secure app](https://docs.mendix.com/howto/security/create-a-secure-app) and [Security Best Practices and Improvements for Mendix Applications](https://docs.mendix.com/howtogeneral/bestpractices/best-practices-security-and-improvements-for-mendix-applications) is assumed. This post only discusses the security defined at entity access level related to generalisations and specialisations.

# Respect my securitah
The Mendix Runtime protects your application and data according to your model. The Mendix developer should define entity access rules in the model, these are applied whenever a query (received from a client) is executed and constrains the data returned to the client.
**The Mendix Runtime will look to the lowest subclass of the object for access rules.** As a general rule; always apply entity access on the specialization when dealing with inheritance. The Mendix Runtime will look for the specialised members of the object to determine which entity access rules should be applied. For security on inheritance: set access rules on the lowest subclasses (specialisation) since security on the superclass (generalisation) is most often ignored.

## How it works:
Mendix stores on the generalisation in an attribute 'submetaobjectname' the subclass of the object.

![submetaobjectname](../imgs/2017/Research_GeneralisationSpecialisationXPaths2.PNG)

Consider the following simplified domain model illustrated below. If we create a car (specialisation) the submetaobjectname will be "MyFirstModule.Car". However if we create a vehicle the value will be "MyFirstModule.Vehicle". When dealing with an object with inheritance the platform will look at the hierarchy and determine the lowest subclass of the object, the security of this subclass is applied.

![Domain model with inheritance](../imgs/2017/Research_GeneralisationSpecialisationXPaths.PNG)

## Considerations:
* Default buttons and Microflow buttons (with apply entity access on) behave differently. E.g. an overview of the generalisation entity "vehicle" with no delete rights, will not show the default delete button. A microflow with a delete action with apply entity access on will still be shown and only delete the object if the specialisation allows it, else "deleting object failed for security reasons".
* In general xpaths on generalisations are not applied to queries when the specialisation is used. Thus, performance wise it should not interfere.
* Not all errors are caught with "failed for security reasons". A microflow with entity access applied will trigger "An error has occurred" when a microflow actions such as change object has insufficient rights. E.g. a microflow with apply entity access, retrieves a different specialisation (of type Motorcyle, with only Read rights) and tries to change and commit the generalisation.
* Check the queries when combining entity access, xPaths on forms, multi-tenant constructions and inheritance. Sometimes Mendix runtime just makes a mess of those queries, which can put a great strain on the application. However this is only applies to more complex cases.
* Specialisations of entities in the System module behave differently (you can't overwrite entity access to attributes from the System module)
