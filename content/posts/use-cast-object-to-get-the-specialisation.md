+++
title = "Use Cast Object to get the specialisation"
description = "Use Cast Object with an inheritance split to get the specialised members of the object. This is much faster."
tags = [
    "Cast object",
    "Inheritance",
]
date = "2016-08-17"
categories = [
    "Mendix",
]
menu = "main"
author = "Stephan Bruijnis"
+++

The Mendix platform queries the entire hierarchy when retrieving the specialisation from the database, in order to guarantee a consistent data structure. When retrieving the generalisation it will also retrieve the entire hierarchy. Thus, developers should never retrieve a specialisation from database if the generalisation is already available. Use Cast Object with an inheritance split to get the specialised members of the object. This is much faster.

## Inheritance
A specialisation is an entity that inherits attributes, associations, events etc. from the generalisation (or super entity). Inheritance can be used for derived functionality such as Users, Images or FileDocuments from the System Module in Mendix or in scenarios were different object types use similar processes. It allows for re-use of functionality and increases the consistency of your application. Inheritance also has drawbacks, in a system with a high transaction volume inheritance slows down the processing.

## Retrieve specialisation
_If you retrieve any specialisations from the super class the platform will always include the entire hierarchy in the query, in order to guarantee a consistent data structure. I.E. if you have an overview of Administration.Account the platform will include the System.User table whether or not you show any System.User attributes, just to make sure that the data is consistent and complete. Both tables have a clustered index on the object id, so joining the information in the database is extremely efficient_ ([Mx](https://world.mendix.com/display/public/bestpractices/Inheritance+vs.+1-1+association)).

## Retrieve generalisation
If you use a generalisation - as a Microflow Parameter, a Retrieve on Database or a Retrieve by Association - the Mendix platform will always include the entire hierarchy in the query. Which means that all specialisations are retrieved, even if they are not needed by the subsequent flow. I.E. if you retrieve a System.User it will also retrieve the Administration.Account specialisation.

Consider a domain model with Car as a specialisation of the entity vehicle, and Convertible as specialisation of Car. The vehicle entity is already available (microflow parameter from the data grid). The developer should use cast object with an inheritance split to get the Convertible object, this is more efficient, since an additional retrieve on the database is prevented. Yet, most developers retrieve the specialisations from the database with an xPath constraint such as `[id = $vehicle]`, which creates a redundant retrieve, thus slowing down performance.

## Implications
Developers should consider the platform implementation of inheritance. If the generalisation object is already available, the developer should always use cast object with inheritance split. This is applicable to `$currentUser`, parsed objects (microflow parameters), as well as retrieved objects.

_Thanks to Bart Rikers for pointing out - on the Mendix Forums - that a Cast object is more efficient than a retrieve_
