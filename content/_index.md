---
type: docs
bookToc: false
---

# Latest posts:

{{< columns >}}

## [What the (c)heck](/posts/what-the-check/)

Code review or peer review is the process of systematically reviewing each others code/work for mistakes, errors and quality. It is a fundamental and highly effective process to increase quality in software development. 

<--->

## [Fast manipulation of large tables](/posts/fast-manipulation-of-large-tables/)

Mendix applications can accumulate large sets of data originating from various sources and created for various reasons. In order to work efficiently with large datasets extra attention to performance is needed. This post will provide recommendations for speed and performance when modelling manipulations of large datasets.

{{< /columns >}}


# Featured post: 

## [The developers guide to performance](/posts/the-developers-guide-to-performance/)

End-users expect fast and engaging web experiences. Yet the application landscape is becoming more complex. Your Mendix application can meet these rising expectations and impress users on both desktop and mobile devices. Learn how to locate and analyse performance issues, improve application performance and gain insight in how design decisions impact your application. This post is a comprehensive reference guide for developers to optimize their applications.

